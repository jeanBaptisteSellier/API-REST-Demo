import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, tap } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isAuth$ = new BehaviorSubject<boolean>(false);
  private userId: string = '';

  constructor(private http: HttpClient,
    private router: Router) {
    const token = this.getTokenFromLocalStorage() || this.getTokenFromCookies();
    if (token) {
      this.isAuth$.next(true);
      const userId = this.getUserIdFromLocalStorage();
      if (userId) {
        this.userId = userId;
      }
    } else {
      this.isAuth$.next(false);
    }
  }

  createUser(email: string, password: string) {
    return this.http.post<{ message: string }>('http://localhost:3000/api/auth/signup', {email: email, password: password});
  }

  getToken() {
    return this.getTokenFromLocalStorage() || this.getTokenFromCookies();
  }

  getTokenFromCookies() {
    const name = 'token=';
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');
    for(let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    console.log("Token not found"); // Log if token cookie is not found
    return "";
  }

  saveTokenToCookies(token: string) {
    document.cookie = `token=${token}; Max-Age=3600; path=/;SameSite=None;Secure`;
  }

  getTokenFromLocalStorage() {
    return localStorage.getItem('token');
  }

  saveTokenToLocalStorage(token: string) {
    localStorage.setItem('token', token);
  }

  saveUserIdToLocalStorage(userId: string) {
    localStorage.setItem('userId', userId);
    console.log('User ID saved to local storage:', userId);
  }

  getUserIdFromLocalStorage() {
    const userId = localStorage.getItem('userId');
    console.log('User ID retrieved from local storage:', userId);
    return localStorage.getItem('userId');

  }

  getUserId() {
    console.log('getUserId called. Current user ID:', this.userId);
    return this.userId;
  }

  loginUser(email: string, password: string) {
    return this.http.post<{ userId: string, token: string }>('http://localhost:3000/api/auth/login', {email: email, password: password}, {withCredentials: true}).pipe(
      tap(({ userId, token }) => {
        this.saveTokenToCookies(token);
        this.saveTokenToLocalStorage(token);
        this.saveUserIdToLocalStorage(userId);
        this.isAuth$.next(true);
        this.userId = userId;
        console.log('Login successful. User ID:', userId);
        console.log('Token saved to local storage:', token);
        console.log('Token saved to cookies:', this.getTokenFromCookies());
      })
    );
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    document.cookie = `token=; Max-Age=0; path=/;SameSite=None;Secure`;
    this.isAuth$.next(false);
    this.router.navigate(['login']);
  }

  getAuthHeaders() {
    const token = this.getToken();
    return new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
  }

  refreshToken() {
    const token = this.getTokenFromLocalStorage() || this.getTokenFromCookies();
    return this.http.post<{ token: string }>('http://localhost:3000/api/token/refresh', { token }, { withCredentials: true }).pipe(
      tap(response => {
        this.saveTokenToLocalStorage(response.token);
        this.saveTokenToCookies(response.token);
      })
    );
  }
}

