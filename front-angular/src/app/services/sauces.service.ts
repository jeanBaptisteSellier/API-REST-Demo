import { Injectable } from '@angular/core';
import { BehaviorSubject, catchError, map, mapTo, of, Subject, tap, throwError } from 'rxjs';
import { Sauce } from '../models/Sauce.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class SaucesService {

  //sauces$ = new Subject<Sauce[]>();
  private saucesSubject = new BehaviorSubject<Sauce[]>([]);
  sauces$ = this.saucesSubject.asObservable();


  constructor(private http: HttpClient,
              private auth: AuthService) {}

  getSauces() {
    const headers = this.auth.getAuthHeaders();
    this.http.get<Sauce[]>('http://localhost:3000/api/sauces', { headers }).pipe(
      tap(sauces => {
        this.saucesSubject.next(sauces);
      }),
      catchError(error => {
        if (error.error && error.error.message) {
          console.error(error.error.message);
        } else {
          console.error('An unexpected error occurred:', error);
        }
        return of([]);
      })
    ).subscribe();
  }

  getSauceById(id: number) {
    return this.http.get<Sauce>('http://localhost:3000/api/sauces/' + id, {
      withCredentials: true
    }).pipe(
      catchError(error => {
        if (error.error && error.error.message) {
          return throwError(error.error.message);
        } else {
          return throwError('An unexpected error occurred');
        }
      })
    );
  }

  likeSauce(id: number, like: boolean) {
    return this.http.post<Sauce>(
      'http://localhost:3000/api/sauces/' + id + '/like',
      { userId: this.auth.getUserId(), like: like ? 1 : 0 }, { withCredentials: true }
    ).pipe(
      tap(updatedSauce => {
        const sauces = this.saucesSubject.getValue();
        const index = sauces.findIndex(sauce => sauce.id === id);
        if (index !== -1) {
          sauces[index] = updatedSauce;
          this.saucesSubject.next([...sauces]);
        }
      }),
      catchError(error => {
        console.error('Error in likeSauce:', error);
        return throwError('An unexpected error occurred')
      })
    );
  }

  dislikeSauce(id: number, dislike: boolean) {
    return this.http.post<Sauce>(
      'http://localhost:3000/api/sauces/' + id + '/like',
      { userId: this.auth.getUserId(), like: dislike ? -1 : 0 }, { withCredentials: true }
    ).pipe(
      tap(updatedSauce => {
        // Mettre à jour la liste des sauces dans le BehaviorSubject
        const sauces = this.saucesSubject.getValue();
        const index = sauces.findIndex(sauce => sauce.id === id);
        if (index !== -1) {
          sauces[index] = updatedSauce;
          this.saucesSubject.next([...sauces]);
        }
      }),
      map(() => dislike), // Renvoie le booléen pour indiquer le succès de l'opération
      catchError(error => {
        if (error.error && error.error.message) {
          return throwError(error.error.message);
        } else {
          return throwError('An unexpected error occurred');
        }
      })
    );
  }


  createSauce(sauce: Sauce, image: File) {
    const formData = new FormData();
    formData.append(
      'sauce',new Blob([JSON.stringify(sauce)], {
        type: 'application/json'
      }) 
    );
    formData.append('image', image);
    formData.append('userId', this.auth.getUserId());
  
    return this.http.post<{ message: string }>('http://localhost:3000/api/sauces', formData, {withCredentials: true}).pipe(
      catchError(error => {
        if (error.error && error.error.message) {
          return throwError(error.error.message);
        } else {
          return throwError('An unexpected error occurred');
        }
      })
    );
  }

  modifySauce(id: number, sauce: Sauce, image: string | File) {
    if (typeof image === 'string') {
      return this.http.put<{ message: string }>('http://localhost:3000/api/sauces/' + id, sauce, {withCredentials: true}).pipe(
        catchError(error => throwError(error.error.message))
      );
    } else {
      const formData = new FormData();
      formData.append('sauce', JSON.stringify(sauce));
      formData.append('image', image);
      return this.http.put<{ message: string }>('http://localhost:3000/api/sauces/' + id, formData,{withCredentials: true}).pipe(
        catchError(error => {
          if (error.error && error.error.message) {
            return throwError(error.error.message);
          } else {
            return throwError('An unexpected error occurred');
          }
        })
      );
    }
  }

  deleteSauce(id: number) {
    return this.http.delete<{ message: string }>('http://localhost:3000/api/sauces/' + id, {withCredentials: true}).pipe(
      catchError(error => {
        if (error.error && error.error.message) {
          return throwError(error.error.message);
        } else {
          return throwError('An unexpected error occurred');
        }
      })
    );
  }
}