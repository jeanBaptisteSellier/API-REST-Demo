import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { SaucesService } from '../services/sauces.service';
import { catchError, map, Observable, of, switchMap, take, tap } from 'rxjs';
import { Sauce } from '../models/Sauce.model';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-sauce-list',
  templateUrl: './sauce-list.component.html',
  styleUrls: ['./sauce-list.component.scss']
})
export class SauceListComponent implements OnInit {
  sauces$!: Observable<Sauce[]>;
  sauce$!: Observable<Sauce>;
  loading!: boolean;
  errorMsg!: string;
  userId!: string;
  likePending!: boolean;
  liked!: boolean;
  disliked!: boolean;
  errorMessage!: string;

  constructor(private sauceServices: SaucesService,
              private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute,
              private cdRef: ChangeDetectorRef) {}

  ngOnInit() {
    this.loading = true;
    this.sauces$ = this.sauceServices.sauces$.pipe(
      tap(sauces => {
        this.loading = false;
        this.errorMsg = '';
      }),
      catchError(error => {
        this.errorMsg = JSON.stringify(error);
        this.loading = false;
        return of([]);
      })
    );
    this.sauceServices.getSauces();

    this.userId = this.authService.getUserId();
    this.loading = true;
    this.userId = this.authService.getUserId();
    this.sauce$ = this.route.params.pipe(
      map(params => params['id']),
      switchMap(id => this.sauceServices.getSauceById(id)),
      tap(sauce => {
        this.loading = false;
        if (sauce.usersLiked.find(user => user === this.userId)) {
          this.liked = true;
        } else if (sauce.usersDisliked.find(user => user === this.userId)) {
          this.disliked = true;
        }
      })
    );
    //this.authService.getTokenFromCookies();
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  onViewSingleSauce(id: number){
    this.router.navigateByUrl(`/sauce/${id}`);
  }

  onLike() {
    if (this.disliked) {
      return;
    }
    this.likePending = true;
    this.sauce$.pipe(
      take(1),
      switchMap((sauce: Sauce) => this.sauceServices.likeSauce(Number(sauce.id), !this.liked).pipe(
        tap(() => {
          this.likePending = false;
          this.liked = !this.liked;
        }),
        tap(updatedSauce => {
          this.sauce$ = of(updatedSauce);
        }),
      catchError(error => {
        this.likePending = false;
        console.error('Error in onLike:', error);
        return of(null);
      })
    ))
    ).subscribe();
  }

  onDislike() {
    if (this.liked) {
      return;
    }
    this.likePending = true;
    this.sauce$.pipe(
      take(1),
      switchMap((sauce: Sauce) => this.sauceServices.dislikeSauce(Number(sauce.id), !this.disliked).pipe(
        tap(disliked => {
          this.likePending = false;
          this.disliked = disliked;
        }),
        map(disliked => ({ ...sauce, dislikes: disliked ? sauce.dislikes + 1 : sauce.dislikes - 1 })),
        tap(sauce => this.sauce$ = of(sauce))
      )),
    ).subscribe();
  }
}
