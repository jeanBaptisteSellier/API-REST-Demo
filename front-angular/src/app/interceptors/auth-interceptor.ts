import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Observable, catchError, switchMap, throwError } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private auth: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authToken = this.auth.getTokenFromCookies();
    let authReq = req;
    if (authToken) {
      authReq = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + authToken),
        withCredentials: true
      });
    }

    return next.handle(authReq).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401 && authToken) {
          // If 401 response is returned, try to refresh the token
          return this.auth.refreshToken().pipe(
            switchMap(() => {
              const newToken = this.auth.getTokenFromCookies();
              const newAuthReq = req.clone({
                headers: req.headers.set('Authorization', 'Bearer ' + newToken),
                withCredentials: true
              });
              return next.handle(newAuthReq);
            }),
            catchError(refreshError => {
              this.auth.logout();
              return throwError(refreshError);
            })
          );
        } else {
          return throwError(error);
        }
      })
    );
  }
}