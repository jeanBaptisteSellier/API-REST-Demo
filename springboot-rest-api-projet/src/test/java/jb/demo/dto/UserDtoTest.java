package jb.demo.dto;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserDtoTest {
	
	@Test
    public void testGetMail() {
        UserDto userDto = new UserDto();
        userDto.setEmail("test@example.com");
        assertEquals("test@example.com", userDto.getEmail());
    }

}
