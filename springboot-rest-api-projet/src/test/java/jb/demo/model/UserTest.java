package jb.demo.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import jb.demo.dto.UserDto;
import jb.demo.mapper.UserMapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class UserTest {
	@Autowired
	private UserMapper userMapper;
	@Test
    public void testGetMail() {
        User user = new User();
        user.setEmail("test@example.com");
        UserDto userDto = this.userMapper.mapUserToUserDto(user);
        assertEquals("test@example.com", userDto.getEmail());
    }


}
