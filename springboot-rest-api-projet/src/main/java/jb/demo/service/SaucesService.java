package jb.demo.service;

import java.util.List;

import jb.demo.dto.SaucesDto;
import jb.demo.model.Sauces;

public interface SaucesService {
	
	SaucesDto createSauce (SaucesDto sauces);
	
	SaucesDto updateSauces (SaucesDto sauces);
	
	List<SaucesDto> getAllSauces();
	
	void deleteSauces (Integer id);
	
	SaucesDto getSauceById (Integer id);

	Boolean likeSauce (Integer id, String userId, Boolean like);

}
