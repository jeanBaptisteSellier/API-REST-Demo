package jb.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import jb.demo.dto.UserDto;
import jb.demo.mapper.UserMapper;
import jb.demo.model.User;
import jb.demo.repository.UsersRepository;

@Service
public class AuthenticationService {
		
		@Autowired
	    private UsersRepository userRepository;
	    
		
	    
		@Autowired
	    private AuthenticationManager authenticationManager;
		
		@Autowired
		private UserMapper userMapper;


	    public UserDto signup(UserDto userDto) {
			User user = userMapper.mapUserDtoToUser(userDto);
			User savedUser = userRepository.save(user);
			return userMapper.mapUserToUserDto(savedUser);
		}

	    public UserDto authenticate(UserDto userDto) {
			try {
				authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					userDto.getEmail(), 
					userDto.getPassword()
				));
			} catch (Exception e) {
				throw new RuntimeException("Invalid credentials", e);
			}
			
			Optional<User> userOptional = userRepository.findByEmail(userDto.getEmail());
			User user = userOptional.orElseThrow(() -> new RuntimeException("User not found"));
			return userMapper.mapUserToUserDto(user);
		}
	}

