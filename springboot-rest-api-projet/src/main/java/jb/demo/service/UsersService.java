package jb.demo.service;

import java.util.List;

import jb.demo.model.User;

public interface UsersService {
	
	User createUser(User users);
	
	User updateUser(User users);
	
	List<User> getAllUsers();
	
	void deleteUser(Integer userId);
	
	User getUserById(Integer userId);

}
