package jb.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jb.demo.exception.ResourceNotFoundException;
import jb.demo.model.User;
import jb.demo.repository.UsersRepository;
import jb.demo.service.UsersService;

@Service
public class UsersServiceImpl implements UsersService {
	
	@Autowired
	private UsersRepository usersRepository;	


	@Override
	public User createUser(User users) {
		return usersRepository.save(users);
	}

	@Override
	public User updateUser(User users) {
		// TODO Auto-generated method stub
		Optional<User> usersData = this.usersRepository.findById(users.getUserId());
		
		if (usersData.isPresent()) {
			User UsersUpdate = usersData.get();
			UsersUpdate.setUserId(users.getUserId());
			UsersUpdate.setEmail(users.getEmail());
			UsersUpdate.setPassword(users.getPassword());
			return usersRepository.save(UsersUpdate);
		} else {
			throw new ResourceNotFoundException("Record not found with id : " + users.getUserId());
		}
	
	}

	@Override
	public List<User> getAllUsers() {
		return this.usersRepository.findAll();
	}

	@Override
	public void deleteUser(Integer userId) {
		Optional<User> usersData = this.usersRepository.findById(userId);
		
		if(usersData.isPresent()) {
			this.usersRepository.delete(usersData.get());
		} else {
			throw new ResourceNotFoundException("Record not found with id : " + userId);
		}

	}

	@Override
	public User getUserById(Integer userId) {
		Optional<User> usersData = this.usersRepository.findById(userId);
		
		if(usersData.isPresent()) {
			return usersData.get();
		} else {
			throw new ResourceNotFoundException("Record not found with id : " + userId);
		}
		
	}

}
