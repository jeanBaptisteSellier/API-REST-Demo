package jb.demo.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import jb.demo.dto.SaucesDto;
import jb.demo.exception.ResourceNotFoundException;
import jb.demo.mapper.SaucesMapper;
import jb.demo.model.Sauces;
import jb.demo.model.User;
import jb.demo.repository.SaucesRepository;
import jb.demo.repository.UsersRepository;
import jb.demo.service.SaucesService;

@Service
public class SaucesServicesImpl implements SaucesService {

	@Autowired
	private SaucesRepository saucesRepository;

	@Autowired
	private UsersRepository usersRepository;

	@Autowired
	private SaucesMapper saucesMapper;

	@Override
	public SaucesDto createSauce(SaucesDto saucesDto) {
		Sauces sauce = saucesMapper.mapSauceDtoToSauces(saucesDto);
		return saucesMapper.mapSaucesToSauceDto(saucesRepository.save(sauce));
	}

	@Override
	public SaucesDto updateSauces(SaucesDto sauceDto) {
		Optional<Sauces> sauceData = this.saucesRepository.findById(sauceDto.getId());

		if (sauceData.isPresent()) {
			Sauces sauceUpdate = sauceData.get();
			sauceUpdate.setId(sauceDto.getId());
			sauceUpdate.setName(sauceDto.getName());
			sauceUpdate.setManufacturer(sauceDto.getManufacturer());
			sauceUpdate.setDescription(sauceDto.getDescription());
			sauceUpdate.setMainPepper(sauceDto.getMainPepper());
			sauceUpdate.setImageUrl(sauceDto.getImageUrl());
			return saucesMapper.mapSaucesToSauceDto(saucesRepository.save(sauceUpdate));
		} else {
			throw new ResourceNotFoundException("Record not found with id : " + sauceDto.getId());
		}
	}

	@Override
	public List<SaucesDto> getAllSauces() {
		List<Sauces> sauces = saucesRepository.findAll();
		return saucesMapper.mapSaucesToSauceDto(sauces);
	}

	@Override
	public void deleteSauces(Integer id) {
		Optional<Sauces> sauceData = this.saucesRepository.findById(id);

		if (sauceData.isPresent()) {
			this.saucesRepository.delete(sauceData.get());
		} else {
			throw new ResourceNotFoundException("Record not found with id : " + id);
		}

	}

	@Override
	public SaucesDto getSauceById(Integer id) {
		Optional<Sauces> sauceData = this.saucesRepository.findById(id);

		if (sauceData.isPresent()) {
			return saucesMapper.mapSaucesToSauceDto(sauceData.get());
		} else {
			throw new ResourceNotFoundException("Record not found with id : " + id);
		}
	}

	@Override
	@Transactional
	public Boolean likeSauce(Integer id, String userId, Boolean like) {
        // Fetch the authenticated user's details
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = authentication.getName();

        User user = usersRepository.findByEmail(userEmail)
                .orElseThrow(() -> new ResourceNotFoundException("User not found with email: " + userEmail));
        
        String userIdInt = user.getUserId().toString();

        Optional<Sauces> sauceOpt = saucesRepository.findById(id);
        if (!sauceOpt.isPresent()) {
            throw new ResourceNotFoundException("Sauce not found with id: " + id);
        }
        Sauces sauce = sauceOpt.get();

        if (like) {
            if (sauce.getUsersLiked() == null) {
                sauce.setUsersLiked(new HashSet<>());
            }
            if (!sauce.getUsersLiked().contains(userIdInt)) {
                sauce.setLikes((sauce.getLikes() == null ? 0 : sauce.getLikes()) + 1);
                sauce.getUsersLiked().add(userIdInt);
            }
        } else {
            if (sauce.getUsersLiked() != null && sauce.getUsersLiked().contains(userIdInt)) {
                sauce.setLikes(sauce.getLikes() - 1);
                sauce.getUsersLiked().remove(userIdInt);
            }
        }

        saucesRepository.save(sauce);
        return true;
    }
}

	/* @Override
public Boolean likeSauce(Integer id, String userId, Boolean like) {
    Optional<Sauces> optionalSauce = saucesRepository.findById(id);
    if (!optionalSauce.isPresent()) {
        throw new ResourceNotFoundException("Sauce not found with id : " + id);
    }
    Sauces sauce = optionalSauce.get();
    if (like) {
        if (sauce.getUsersLiked() == null) {
            sauce.setUsersLiked(new HashSet<>());
        }
        if (!sauce.getUsersLiked().contains(userId)) {
            sauce.setLikes((sauce.getLikes() == null ? 0 : sauce.getLikes()) + 1);
            sauce.getUsersLiked().add(userId);
        }
    }
    saucesRepository.save(sauce);
    return true;
}
 */

