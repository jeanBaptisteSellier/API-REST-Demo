package jb.demo.response;

public class LoginResponse {
	
    private String token;

	private Integer userId;

    private long expiresIn;
    
    public LoginResponse() {
    	super();
    }

    public LoginResponse(String token, Integer userId, long expiresIn) {
		super();
		this.token = token;
		this.userId = userId;
		this.expiresIn = expiresIn;
	}

	public String getToken() {
        return token;
    }

	public Integer getUserId() {
		return userId;
	}

	public long getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(long expiresIn) {
		this.expiresIn = expiresIn;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setToken(String token) {
		this.token = token;
	}
    
    
}

