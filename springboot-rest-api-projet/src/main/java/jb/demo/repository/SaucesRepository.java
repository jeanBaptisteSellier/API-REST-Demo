package jb.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jb.demo.model.Sauces;

@Repository
public interface SaucesRepository extends JpaRepository<Sauces, Integer> {

}
