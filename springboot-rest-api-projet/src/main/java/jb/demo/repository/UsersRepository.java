package jb.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jb.demo.model.User;

@Repository
public interface UsersRepository extends JpaRepository<User, Integer> {
	
	Optional<User> findByEmail(String email);
}
