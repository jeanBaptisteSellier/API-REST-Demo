package jb.demo.dto;


public class UserDto {
	
	private Integer userId;

	private String password;

	private String email;

	public UserDto() {
	}

	public UserDto(Integer userId, String password, String email) {
		super();
		this.userId = userId;
		this.password = password;
		this.email = email;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	

}

