package jb.demo.dto;

public class LikeRequest {
    private String userId;
    private Boolean like;

    public LikeRequest() {
    }   

    public LikeRequest(String userId, Boolean like) {
        super();
        this.userId = userId;
        this.like = like;
    }

    public String getUserId() {
        return userId;
        }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Boolean isLike() {
        return like;
    }

    public void setLike(Boolean like) {
        this.like = like;
    }
    
}
