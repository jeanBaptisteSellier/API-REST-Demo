package jb.demo.dto;

import java.util.List;
import java.util.Set;

public class SaucesDto {

    private Integer id;
    private String name;
    private String description;
    private String imageUrl;
    private String mainPepper;
    private Integer heat;
    private Integer likes;
    private Integer dislikes;
    private String manufacturer;
    private Set<String> usersLiked;
    private Set<String> usersDisliked;
    private String userId;

    public SaucesDto() {
    }

    public SaucesDto(Integer id, String name, String description, String imageUrl, String mainPepper, Integer heat, Integer likes, Integer dislikes, String manufacturer, String userId, Set<String> usersLiked, Set<String> usersDisliked) {
        super();
        this.id = id;
        this.name = name;
        this.description = description;
        this.imageUrl = imageUrl;
        this.mainPepper = mainPepper;
        this.heat = heat;
        this.likes = likes;
        this.dislikes = dislikes;
        this.manufacturer = manufacturer;
        this.usersLiked = usersLiked;
        this.usersDisliked = usersDisliked;
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getMainPepper() {
        return mainPepper;
    }

    public void setMainPepper(String mainPepper) {
        this.mainPepper = mainPepper;
    }

    public Integer getHeat() {
        return heat;
    }

    public void setHeat(Integer heat) {
        this.heat = heat;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getDislikes() {
        return dislikes;
    }

    public void setDislikes(Integer dislikes) {
        this.dislikes = dislikes;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Set<String> getUsersLiked() {
        return usersLiked;
    }

    public void setUsersLiked(Set<String> usersLiked) {
        this.usersLiked = usersLiked;
    }

    public Set<String> getUsersDisliked() {
        return usersDisliked;
    }

    public void setUsersDisliked(Set<String> usersDisliked) {
        this.usersDisliked = usersDisliked;
    }
    
}
