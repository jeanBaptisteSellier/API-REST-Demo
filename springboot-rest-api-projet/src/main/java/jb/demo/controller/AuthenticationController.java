package jb.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletResponse;
import jb.demo.config.security.UserDetailsImpl;
import jb.demo.dto.UserDto;
import jb.demo.model.User;
import jb.demo.response.LoginResponse;
import jb.demo.service.AuthenticationService;
import jb.demo.service.JwtService;

@RequestMapping("/api/auth")
@RestController
@CrossOrigin(origins = "http://localhost:4200", allowCredentials = "true")
public class AuthenticationController {
	
	
	@Autowired
    private JwtService jwtService;
    
	@Autowired
    private AuthenticationService authenticationService;


	@PostMapping(path = "/signup", consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<UserDto> register(@RequestBody UserDto userDto) {
        UserDto registeredUser = authenticationService.signup(userDto);

        return ResponseEntity.ok(registeredUser);
	}

	@PostMapping(path = "/login", consumes = {MediaType.APPLICATION_JSON_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<LoginResponse> authenticate(@RequestBody UserDto userDto, HttpServletResponse response) {
        UserDto authenticatedUser = authenticationService.authenticate(userDto);
        UserDetailsImpl userDetails = new UserDetailsImpl(authenticatedUser);
        String jwtToken = jwtService.generateToken(userDetails);
        Integer userId = authenticatedUser.getUserId();

       ResponseCookie cookie = ResponseCookie.from("token", jwtToken)
        .httpOnly(false)
        .secure(false) // Marque le cookie comme secure
        .domain("localhost")
        .path("/")
        .maxAge(jwtService.getExpirationTime())
        .sameSite("None") // Définit SameSite=None
        .build();

        response.addHeader(HttpHeaders.SET_COOKIE, cookie.toString());

        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setToken(jwtToken);
        loginResponse.setUserId(userId);
        loginResponse.setExpiresIn(jwtService.getExpirationTime());

        return ResponseEntity.ok()
                .header(HttpHeaders.SET_COOKIE, cookie.toString())
                .body(loginResponse);
    }
}
