package jb.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jb.demo.service.JwtService;

@RestController
@RequestMapping("/api/token")
public class TokenController {

    @Autowired
    private JwtService jwtService;

    @PostMapping("/refresh")
    public ResponseEntity<?> refreshToken(@RequestHeader("Authorization") String refreshToken) {
        try {
            String newJwt = jwtService.refreshToken(refreshToken.substring(7));
            return ResponseEntity.ok(newJwt);
        } catch (Exception e) {
            return ResponseEntity.status(401).body("Token de rafraîchissement invalide ou expiré");
        }
    }
}
