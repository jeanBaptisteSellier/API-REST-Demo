package jb.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import jb.demo.model.User;

@RestController
@RequestMapping("/api/auth")
public class UsersRestController {
	
	@Autowired
	private jb.demo.service.UsersService usersService;
	
	@GetMapping("/")
	public ResponseEntity<String> defaultEndpoint() {
	    return ResponseEntity.ok("Welcome to the authentication API!");
	}

	
	/*@PostMapping("/users")
	public ResponseEntity<User> create(@RequestBody User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		return ResponseEntity.ok().body(this.usersService.createUser(user));
	}*/
	
	
	@GetMapping("/users")	
	public ResponseEntity<List<User>> getAllUsers(){
		return ResponseEntity.ok().body(usersService.getAllUsers());
	}
	
	@GetMapping("/users/{id}")
	public ResponseEntity <User> getUserById(@PathVariable Integer userId){
		return ResponseEntity.ok().body(usersService.getUserById(userId));
	}
	
	@PutMapping("/users/{id}")
	public ResponseEntity <User> updateUser(@PathVariable Integer userId, @RequestBody User user){
		user.setUserId(userId);
		return ((BodyBuilder) ResponseEntity.ok(null)).body(this.usersService.updateUser(user));
		
	}
	
	@DeleteMapping("/users/{userId}")
	public HttpStatus deleteUser(@PathVariable Integer userId) {
		this.usersService.deleteUser(userId);
		return HttpStatus.OK;
	}
	
}
	

	
