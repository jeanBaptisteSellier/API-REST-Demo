package jb.demo.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import jb.demo.dto.LikeRequest;
import jb.demo.dto.SaucesDto;
import jb.demo.model.Sauces;
import jb.demo.service.SaucesService;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:4200",  allowCredentials = "true")
public class SaucesRestController {

	@Autowired
	private SaucesService saucesService;

	//création d'une sauce
    @PostMapping(path = "/sauces", consumes = "multipart/form-data")
    public ResponseEntity<SaucesDto> createSauce(@RequestPart("sauce") String sauceJson, @RequestPart("image") MultipartFile image, @RequestParam("userId") String userId) {
        try {
            // Convertir le JSON en objet Sauce
            ObjectMapper objectMapper = new ObjectMapper();
            SaucesDto sauceDto = objectMapper.readValue(sauceJson, SaucesDto.class);
          
            sauceDto.setUserId(userId);
            
            // Vérifier si l'image est fournie
            if (image != null && !image.isEmpty()) {
                // Récupérer le nom de fichier original
                String fileName = StringUtils.cleanPath(image.getOriginalFilename());

                // Définir le répertoire où stocker les images (par exemple, sur le bureau)
                String uploadDir = System.getProperty("user.home") + "/Desktop/sauce-images";

                // Créer le répertoire s'il n'existe pas déjà
                File directory = new File(uploadDir);
                if (!directory.exists()) {
                    directory.mkdirs();
                }

                // Déplacer l'image vers le répertoire de stockage
                Path filePath = Paths.get(uploadDir + File.separator + fileName);
                Files.copy(image.getInputStream(), filePath);

                // Mettre à jour l'URL de l'image dans l'objet Sauce
                sauceDto.setImageUrl(fileName);
            } else {
                // Gérer le cas où aucune image n'est fournie
                sauceDto.setImageUrl(null);
            }

            // Enregistrer l'objet Sauce dans la base de données
            SaucesDto savedSauce = saucesService.createSauce(sauceDto);
            return ResponseEntity.ok().body(savedSauce);

        } catch (IOException ex) {
            // Gérer les erreurs de traitement de l'image ou de conversion JSON
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }

    //récupérer toutes les sauces
	@GetMapping(path = "/sauces", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<List<SaucesDto>> getAllSauces() {
		return ResponseEntity.ok().body(saucesService.getAllSauces());
	}

    //récupérer une sauce par son id
	@GetMapping("/sauces/{id}")
	public ResponseEntity<SaucesDto> getSaucesById(@PathVariable(name = "id") Integer id) {
		return ResponseEntity.ok().body(saucesService.getSauceById(id));
	}

    //modification d'une sauce
	@PutMapping("/sauces/{id}")
	public ResponseEntity<SaucesDto> updateSauce(@PathVariable Integer id, @RequestBody SaucesDto sauce) {
		sauce.setId(id);
		return ResponseEntity.ok().body(this.saucesService.updateSauces(sauce));
	}

    //ajout d'un like 
    @PostMapping("/sauces/{id}/like")
    public ResponseEntity<?> likeSauce(@PathVariable Integer id, @RequestBody LikeRequest likeRequest) {
        boolean success = saucesService.likeSauce(id, likeRequest.getUserId(), likeRequest.isLike());
        if (success) {
            SaucesDto updatedSauce = saucesService.getSauceById(id);
            return ResponseEntity.ok().body(updatedSauce);
        } else {
            return ResponseEntity.status(400).body("Error liking/disliking sauce");
        }
    }
    

}
