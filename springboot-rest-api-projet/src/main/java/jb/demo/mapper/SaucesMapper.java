package jb.demo.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jb.demo.dto.SaucesDto;
import jb.demo.model.Sauces;
import jb.demo.model.User;
import jb.demo.repository.UsersRepository;

@Component
public class SaucesMapper {
    
    private User user;

    public SaucesDto mapSaucesToSauceDto(Sauces sauce) {
        SaucesDto sauceDto = new SaucesDto();
        sauceDto.setId(sauce.getId());
        sauceDto.setName(sauce.getName());
        sauceDto.setDescription(sauce.getDescription());
        sauceDto.setImageUrl(sauce.getImageUrl());
        sauceDto.setManufacturer(sauce.getManufacturer());
        sauceDto.setMainPepper(sauce.getMainPepper());
        sauceDto.setHeat(sauce.getHeat());
        sauceDto.setLikes(sauce.getLikes());
        sauceDto.setDislikes(sauce.getDislikes());
        sauceDto.setUserId(sauce.getUserId());
        sauceDto.setUsersLiked(sauce.getUsersLiked());
        sauceDto.setUsersDisliked(sauce.getUsersDisliked());
        return sauceDto;
    }

    public Sauces mapSauceDtoToSauces(SaucesDto sauceDto) {
        Sauces sauce = new Sauces();
        sauce.setId(user.getUserId());
        sauce.setName(sauceDto.getName());
        sauce.setDescription(sauceDto.getDescription());
        sauce.setImageUrl(sauceDto.getImageUrl());
        sauce.setManufacturer(sauceDto.getManufacturer());
        sauce.setMainPepper(sauceDto.getMainPepper());
        sauce.setHeat(sauceDto.getHeat());
        sauce.setLikes(sauceDto.getLikes());
        sauce.setDislikes(sauceDto.getDislikes());
        sauce.setUserId(sauceDto.getUserId());
        sauce.setUsersLiked(sauceDto.getUsersLiked());
        sauce.setUsersDisliked(sauceDto.getUsersDisliked());
        return sauce;
    }

    public List<SaucesDto> mapSaucesToSauceDto(List<Sauces> sauces) {
        return sauces.stream()
                .map(this::mapSaucesToSauceDto)
                .collect(Collectors.toList());
    }
}
