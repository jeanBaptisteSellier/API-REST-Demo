package jb.demo.mapper;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import jb.demo.dto.UserDto;
import jb.demo.model.User;

@Component
public class UserMapper {

	@Autowired
	    private PasswordEncoder passwordEncoder;

		public UserDto mapUserToUserDto(User user) {
			UserDto userDto = new UserDto();
			userDto.setUserId(user.getUserId());
			userDto.setPassword(user.getPassword());
			userDto.setEmail(user.getEmail());
			return userDto;
		}

		public User mapUserDtoToUser(UserDto userDto) {
			User user = new User();
			user.setUserId(userDto.getUserId());
			user.setPassword(passwordEncoder.encode(userDto.getPassword()));
			user.setEmail(userDto.getEmail());
			return user;
		}

	}
