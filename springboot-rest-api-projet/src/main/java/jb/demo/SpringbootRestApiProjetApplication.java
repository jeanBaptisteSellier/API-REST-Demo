package jb.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootRestApiProjetApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootRestApiProjetApplication.class, args);
	}

}
