package jb.demo.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {
	
	@Autowired
    private AuthenticationProvider authenticationProvider;
	
	@Autowired
    private JwtAuthenticationFilter jwtAuthenticationFilter;

    
	 @Bean
	 SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		 http.csrf(csrf -> csrf
		            .disable())
		 			.cors().and()
		            .authorizeHttpRequests(requests -> requests
							.requestMatchers("/swagger-ui.html", "/swagger-ui/**", "/v3/api-docs/**").permitAll()
		                    .requestMatchers("/api/auth/**").permitAll()
		                    .requestMatchers("/api/sauces/**").authenticated()
		                    .anyRequest().authenticated())
		            .sessionManagement(management -> management
		                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS))
		            .authenticationProvider(authenticationProvider)
		            .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);

		    return http.build();
	 }
	 

	 @Bean
	 CorsConfigurationSource corsConfigurationSource() {
	        CorsConfiguration configuration = new CorsConfiguration();
	        configuration.setAllowedOrigins(List.of("http://localhost:4200"));
	        configuration.setAllowedMethods(List.of("GET", "POST", "PUT", "DELETE", "OPTIONS"));
	        configuration.setAllowedHeaders(List.of("Authorization", "Content-Type"));
			configuration.setAllowCredentials(true);

	        CorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	        ((UrlBasedCorsConfigurationSource) source).registerCorsConfiguration("/**", configuration);

	        return source;
	    }
	 
	 @Bean
	 MultipartResolver multipartResolver() {
	        return new StandardServletMultipartResolver();
	    }
}