package jb.demo.config;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jb.demo.service.JwtService;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {
	
	@Autowired
	private JwtService jwtService;
	    
	@Autowired
	private HandlerExceptionResolver handlerExceptionResolver;

	@Autowired
	private UserDetailsService userDetailsService;

	    @Override
	    protected void doFilterInternal(
	        @NonNull HttpServletRequest request,
	        @NonNull HttpServletResponse response,
	        @NonNull FilterChain filterChain
	    ) throws ServletException, IOException {
			String jwt = null;
        String userEmail = null;

        // Check Authorization header first
        final String authHeader = request.getHeader("Authorization");
        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            jwt = authHeader.substring(7);
			try{
            userEmail = jwtService.extractUsername(jwt);
			} catch (ExpiredJwtException e) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token expiré");
                return;
			}
        } else {
            // Check cookies if Authorization header is not present
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("token")) {
                        jwt = cookie.getValue();
                        userEmail = jwtService.extractUsername(jwt);
                        break;
                    }
                }
            }
        }

        if (userEmail != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = this.userDetailsService.loadUserByUsername(userEmail);

            if (jwtService.validateToken(jwt, userDetails)) {
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities()
                );
                authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
        }

        filterChain.doFilter(request, response);
    }
}
	        /*final String authHeader = request.getHeader("Authorization");

	        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
	            filterChain.doFilter(request, response);
	            return;
	        }

	        try {
	            final String jwt = authHeader.substring(7);
	            final String userEmail = jwtService.extractUsername(jwt);

	            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

	            if (userEmail != null && authentication == null) {
	                UserDetails userDetails = this.userDetailsService.loadUserByUsername(userEmail);

	                if (jwtService.isTokenValid(jwt, userDetails)) {
	                    UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
	                            userDetails,
	                            null,
	                            userDetails.getAuthorities()
	                    );

	                    authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
	                    SecurityContextHolder.getContext().setAuthentication(authToken);
	                }
	            }

	            filterChain.doFilter(request, response);
	        } catch (Exception exception) {
	            handlerExceptionResolver.resolveException(request, response, null, exception);
	        }
	    }
	}*/
