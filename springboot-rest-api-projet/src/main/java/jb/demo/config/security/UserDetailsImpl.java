package jb.demo.config.security;

import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import jb.demo.dto.UserDto;

public class UserDetailsImpl extends UserDto implements UserDetails {

	private UserDto userDto;
	
	
	public UserDetailsImpl() {
		super();
	}

	public UserDetailsImpl(UserDto userDto){
		this.userDto = userDto;
	}

	@Override
	public String getPassword() {
		return userDto.getPassword();
	}
	

	@Override
	public String getUsername() {
		return userDto.getEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("Unimplemented method 'getAuthorities'");
	}
}