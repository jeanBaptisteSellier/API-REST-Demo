package jb.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;

@Configuration
public class OpenApiConfiguration {

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
            .info(apiInfo())
            .addSecurityItem(new SecurityRequirement().addList("BearerAuth"));
    }

    private Info apiInfo() {
        return new Info()
                .title("API Test")
                .description("Test API for this article")
                .version("2.0")
                .contact(apiContact());
    }

    private Contact apiContact() {
        return new Contact()
                .name("Jean-Baptiste Sellier")
                .email("jean-baptiste.sellier@hotmail.fr")
                .url("https://gitlab.com/jeanBaptisteSellier");
    }
}
