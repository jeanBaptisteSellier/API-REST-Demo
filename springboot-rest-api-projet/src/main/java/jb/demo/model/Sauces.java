package jb.demo.model;

import java.util.Set;

import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Table;

@Entity
@Table(name = "sauces")
public class Sauces {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = true)
    private String name;

    @Column(nullable = true)
    private String manufacturer;

    @Column(nullable = true, length = 1000)
    private String description;

    @Column(nullable = true)
    private Integer heat;

    @Column(nullable = false)
    private Integer likes = 0;

    @Column(nullable = false)
    private Integer dislikes = 0;

    @Column(nullable = true)
    private String mainPepper;

    @Column(nullable = true)
    private String imageUrl;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "sauce_user_likes", joinColumns = @JoinColumn(name = "sauce_id"))
    @Column(name = "user_id",nullable = true)
    private Set<String> usersLiked;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "sauce_user_dislikes", joinColumns = @JoinColumn(name = "sauce_id"))
    @Column(name = "user_id",nullable = true)
    private Set<String> usersDisliked;

    @Column(name = "user_id", nullable = true)
    private String userId;

    public Sauces() {}

    public Sauces(Integer id, String name, String manufacturer, String description, Integer heat,
                  Integer likes, Integer dislikes, String mainPepper, String imageUrl, Set<String> usersLiked, Set<String> usersDisliked, String userId) {
        this.id = id;
        this.name = name;
        this.manufacturer = manufacturer;
        this.description = description;
        this.heat = heat;
        this.likes = likes;
        this.dislikes = dislikes;
        this.mainPepper = mainPepper;
        this.imageUrl = imageUrl;
        this.usersLiked = usersLiked;
        this.usersDisliked = usersDisliked;
        this.userId = userId;
    }

    // Getters and setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getHeat() {
        return heat;
    }

    public void setHeat(Integer heat) {
        this.heat = heat;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getDislikes() {
        return dislikes;
    }

    public void setDislikes(Integer dislikes) {
        this.dislikes = dislikes;
    }

    public String getMainPepper() {
        return mainPepper;
    }

    public void setMainPepper(String mainPepper) {
        this.mainPepper = mainPepper;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Set<String> getUsersLiked() {
        return usersLiked;
    }

    public void setUsersLiked(Set<String> usersLiked) {
        this.usersLiked = usersLiked;
    }

    public Set<String> getUsersDisliked() {
        return usersDisliked;
    }

    public void setUsersDisliked(Set<String> usersDisliked) {
        this.usersDisliked = usersDisliked;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}