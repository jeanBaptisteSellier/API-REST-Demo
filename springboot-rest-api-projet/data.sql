

CREATE TABLE users (
    id SERIAL NOT NULL,
    email VARCHAR(255),
    password VARCHAR(255),
    roles VARCHAR(255),
    PRIMARY KEY (id)
);

-- Création de la table roles
CREATE TABLE roles (
    id SERIAL NOT NULL,
    role_name VARCHAR(255),
    PRIMARY KEY (id)
);

-- création de la table sauces
create table sauces (
	id SERIAL NOT NULL,
	name VARCHAR(255),
	manufacturer VARCHAR(255),
	description VARCHAR(255),
	heat INT,
	likes int,
	dislikes INT,
	mainPepper VARCHAR(255),
	imageUrl VARCHAR(255),
	user_id INT,
	FOREIGN KEY (user_id) REFERENCES users(id)
);