Ceci est un exercice visant à créer une API REST et de la faire communiquer avec un front Angular.
J'ai choisi de la développer avec Java et Springboot.
J'utilise une authentification par tokens.
Swagger fournit la documentation de l'API.
